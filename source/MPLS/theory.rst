======
Theory
======

.. sectionauthor:: Tihon Gautier && Meyers Quentin

.. raw:: html

    <style> .red {color:red} </style>
    <style> .green {color:green} </style>
    <style> .orange {color:orange} </style>
    <style> .blue {color:blue} </style>
    <style> .gray {color:gray} </style>
    <style> .purple {color:purple} </style>


Useful definitions/acronyms
=============================

**MPLS :** MultiProtocol Label Switching.

**MPLS domain :** consists of a contiguous, or connected, set of MPLS-enabled routers. Traffic can enter or exit the domain.

**Label :** it is a kind of flag attached to the packets. They are used by routers to handle packets (forwarding,...). Labels define flows between two endpoints or, in the case of multicast, between a source endpoint and a multicast group of a destination.

**Label Forwarding Table (LFT) :** is a table used in the packet forwarding. It maps each incoming label to the actions that must be execute in order to forward the packet.

**Label Switching Router (LSR) :** is a MPLS-enabled router. It can add and remove labels to packets before forwarding them according to the incoming packet's label.

**Forwarding Equivalence Class (FEC) :** is a set of packets that will be forwarded in the same way through the network, with the same QoS requirements.

**Label Switched Path (LSP) :** is the unidirectional path followed by packets belonging to a given FEC in the MPLS domain.

**Per-Hop Behaviour (PHB) :** is all the rules that governs how packets are handled when traversing a hop (as a router)


Introduction of MPLS
===================

Multiprotocol Label Switching (MPLS) is a mechanism that directs data from one network node to the next based on short path labels, avoiding complex lookups in a routing table. It reduces the amount of per-packet processing at each router in an IP-based network, enhancing router performances even more.
MPLS brings improvements on these 4 areas :

- QoS support
- traffic engineering
- Virtual Private Networks (VPNs)
- multiprotocol support

We will shortly develop them later.

Forwarding equivalence classes
==============================

Before going further, we will quickly explain you what is Forwarding Equivalence Classes (FEC) and why it is used. A FEC defines a set of packets with similar and/or identical characteristics that will be forwarded in the same way through the network, with the same QoS requirements. In other words, we can group packets according to some of their characteristics into FECs, which allows to handle them easier. Indeed, we can defined forwarding rules for each FEC instead of handling each packet as it was unique. It is a kind of abstraction.

The following figure shows two paths defined for two FECs.

.. tikz::
		:libs: positioning, matrix, arrows
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		
		
		\matrix[row sep=1cm, column sep=1cm] {
			\node[router] (R1) {R1}; & & & & \node[router] (R6) {R6}; & \\
			& & \node[router] (R3) {R3};\\
			\node[router] (R2) {R2}; & & & & \node[router] (R5) {R5}; & \\
		};
		
		\draw[thick] (R1) -- (R6) node[red,midway,sloped,above] {$\longrightarrow$};
		
		\draw[thick] (R1) -- (R3) node[red,midway,sloped,above] {$\longleftarrow$};
		
		\draw[thick] (R3) -- (R6);
		
		\draw[thick] (R2) -- (R3) node[red,midway,sloped,above] {$\longrightarrow$};
		
		\draw[thick] (R5) -- (R6) node[red,midway,sloped,above] {$\longleftarrow$};
		
		\draw[thick] (R2) -- (R5) node[green,midway,sloped,above] {$\longrightarrow$};
		
In order to manage these two paths, we simply need two entries in the forwarding table of R2 (the push function put a label on the packet, this is explained later) :

.. role:: red
.. role:: green

- :red:`if(FEC1) Push(1), North (red arrow)`
- :green:`if(FEC2) Push(2), East (green arrow)`

It is really easier than having a forwarding table with an entry for each packet.

How does MPLS work globally ?
===========================

**Configuring paths** First of all, a path through the network, known as a Label Switched Path (LSP), must be defined for each FEC and the QoS parameters along that path must be established.

**FEC attribution to packet** When a packet enters an MPLS domain through an ingress edge LSR (label switched router),  it is assigned to a particular FEC. The ingress edge LSR appends the appropriate label to the packet, and forwards it. If no LSP yet exists for this FEC, the edge LSR must cooperate with the other LSRs in defining a new LSP.

**Forwarding** Within the MPLS domain, each LSR manages the packet according to its label and then forwards it.

**Leaving MPLS domain** The egress edge LSR strips the label, reads the IP packet header, and forwards the packet to its final destination.

Configuring paths
-----------------

The QoS parameters defined for the FEC determine :

	- how many resources to commit to the path.
	- what queuing and discarding policy to establish at each LSR for packets in this FEC.

To determine the path, two protocols are used :

	- an interior routing protocol (e.g. OSPF) used to exchange reachability and routing information.
	- and one to determine the labels that will be used at each LSR, i.e. the labels that will be attached to the packets according to their FEC, current label,... It can be configured manually, or using an other protocol (e.g. Label Distribution Protocol or an enhanced version of RSVP)

FEC attribution to packets
--------------------------

The FEC for a packet can be determined by one or more of a number of parameters. e.g. :
	- Source/destination IP addresses or IP network addresses
	- source/destination port numbers
	- IP protocol ID
	- Differentiated services codepoint
	- IPv6 flow label

Forwarding
----------

There is three operations on the label :

- PUSH : This operation push a label on the stack of labels.
- POP  : This operation pop the label on the top of the stack.
- SWAP : This operation replace the label on the top of the stack by an another one.

Forwarding is achieved by doing simple lookup in a predefined table that maps label values to next-hop addresses. A particular Per-Hop Behaviour (PHB) can be defined at LSR for a given FEC. The PHB is all the rules that governs how packets are handled when traversing a hop (as a router). The PHB defines the queuing priority of the packets for this FEC and the discard policy.

MPLS header and label
=====================

MPLS packet header
------------------

.. image:: images/packet_MPLS.png
	:scale: 75
	:alt: packet in MPLS
	
**Label (20 bits) :** is used to forward the packet.

**E (3 bits) :** the Experimental bits. This field helps define the QoS PHB.  It's equivalent to DiffServ Code Point (DSCP) in the IP network. 

**S (1 bit) :** the Stack bit. It is put to know where is the end when we have several successive labels on the packet.

**TTL (Time To Live, 8 bits) :** the lifetime of the packet.


Time To Live (TTL) 
''''''''''''''''''
It is used to be able to implement the normal feature of IP layer without touching at the IP header.

How it works ? :

**At ingress edge LSR**

When an IP packet arrives at an ingress edge LSR of an MPLS domain, a single label stack entry is added to the packet.
The TTL value of this label stack entry is set to the value of the IP TTL value.
If the IP TTL field needs to be decremented, as part of the IP processing, it is assumed it has already been done.

**In the MPLS domain**

When an MPLS packet arrives at an internal LSR of an MPLS domain, the TTL value on the top label stack entry is decremented

	- if TTL == 0, then the LSR sends an ICMP hop limit exceeded instead of the packet (along the same LSP as this LSR has no knowledge of how to reach the originator of the packet).
	- else the TTL of the outgoing label is set to the decremented TTL value.

**At egress edge LSR**

When an MPLS packet arrives at an egress edge LSR of an MPLS domain, the TTL value is decremented and the label is popped.

	- if TTL == 0 : ICMP hop limit exceeded or simply discarded.
	- otherwise : TTL of the IP header is set to this value.


Packet Labels
-------------

An MPLS network or internet consists of a set of nodes, called Label Switched Routers (LSRs), that are capable of switching and routing packets on the basis of a label which has been appended to them.

The label is added when the packet enters in the network, and it is removed when the packet leaves the network.
We can put as many label as we want, it has no limit in theory. The labels are stored in a LIFO stack (always processed by the top).

Globally unique labels
''''''''''''''''''''''

Labels are unique on the domain scope. They have global significance.

The advantages are :

- We can force a particular route manually.
- LSRs have the same label forwarding table (LFT) size.

It is used in a new way : Segment routing.

We could have :

- 1 label = 1 path
- 1 label = 1 router (metrics are sent via OSPF (OSPF to compute LFT)). Forward based on labels is faster.
- 1 stack = 1 path.

For more information, you can read the chapter dedicated to it (SR)

Locally unique labels
'''''''''''''''''''''

In default MPLS deployment, labels are assigned per LSP/FEC. In other words, labels are not unique for the domain scope, but only for the router scope. They  have only local significance.

The drawback of this solution is there are different routing tables.

The advantage is that, with this solution, it's possible to support routers with different Label Forwarding Table sizes.

The number of LSP will be limited by the memory for LFT, and the router that has the lowest size will also limit.

How does it works locally on a router ?
======================================


.. image:: images/Label_processing_exemple.png
	:scale: 50
	:alt: packet in MPLS

Each color is a packet with its own labels (the top of the label stack is at the right of the packet). So, you have 5 different packets, to be forwarded to different places according to labels.

.. role:: red
.. role:: blue
.. role:: green
.. role:: purple

- :red:`The red packet has as stack label "L2|L3", so we have to find a match with L3 (L3 is on the top of the stack) in the forwarding table of R3. We find we have to forward the packet in the North-East direction, and we have to SWAP L3 with L0. It's why it's R7 that receives the packet with the stack label "L2|L0".`
- :blue:`For the blue packet, we need to find a match with L4 in the forwarding table of R3. For L4, we have to forward the packet in the East direction, and POP the L4. So, it's R6 that receives the packet.`
- Black packet : L5 -> (South-East and PUSH(L1)) -> R8 receives the packet, with L1 added on the top of the stack of the label.
- :green:`For the green packet, we see we have to keep it locally, but we have also to POP the label on the top of the stack. So, we remove L6, and we check again the table to know where we have to forward the packet. We know that with L3, we have to forward the packet to R7, and replace L3 by L0.`
- :purple:`With L9 (purple packet), we have to send it to South-East direction, and to do 2 operations. First, we have to SWAP L9 by L8, and after, we have to PUSH L4 on the top of the stack. It's why it's R8 that receives the packet with stack label "L8|L4"`

Label forwarding table (LFT)
----------------------------

The following table is a per interface forwarding table. It means that we take into account the interface by which the packet is arrived. Labels are unique only for their interface. Same labelled packets arriving at different interfaces can be handled differently. The index is therefore composed of the Inlabel and the Inport.

+------------------+---------+-------------+
| ______Index_____ |         |             |
+---------+--------+---------+-------------+
| Inlabel | Inport | Outport |  Operation  |
+---------+--------+---------+-------------+
| 20 bits | 3 bits | 3 bits  | 2 + 20 bits |
+---------+--------+---------+-------------+

The maximum size of this table is :math:`2^\text{23}`.

This second table is a per-node forwarding table. It means that whatever is the interface by which the packet is arrived, it will be handled in the same way.

+---------+---------+-------------+
| Inlabel | Outport |  Operation  |
| (Index) |         |             |
+---------+---------+-------------+
| 20 bits | 3 bits  | 2 + 20 bits |
+---------+---------+-------------+

The maximum size the this table is :math:`2^\text{20}`.

The second table is preferred because it's easier to rewrite the routes.

*Can an LSR that has a table a 1000 entries interoperate with an LSR having a table of 1000000 or 100 entries ?*
	Yes, it's possible. As long as the input is interpretable by the table, it works.


Label Stacking
==============

Imagine we want to manage a tunnel, as represented in the picture below.

.. tikz:: Label stacking
		:libs: positioning, matrix, arrows, shapes.geometric, calc, fit
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		
		
		\matrix[row sep=1cm, column sep=1cm] {
			\node[router] (R7) {R7}; & & & & \node[router] (R8) {R8}; & \\
			& & \node[router] (R6) {R6};\\
			\node[router] (R1) {R1}; & & & & \node[router] (R5) {R5};\\
			& \node[router] (R2) {R2}; & & \node[router] (R4) {R4}; & \\
			& \node[router] (R3) {R3};\\
		};
		
		\draw[thick] (R7) -- (R6) node[red,midway,sloped,above] {$\longleftarrow$};
		
		\draw[thick] (R6) -- (R8) node[blue,midway,sloped,above] {$\longrightarrow$};
		
		\draw[thick] (R1) -- (R6);
		
		\draw[thick] (R6) -- (R5) node[red,midway,sloped,above] {$\longleftarrow$} node[blue,midway,sloped,below] {$\longleftarrow$};
		
		\draw[thick] (R1) -- (R2) node[blue,midway,sloped,above] {$\longrightarrow$};
		
		\draw[thick] (R2) -- (R4);
		
		\draw[thick] (R4) -- (R5);
		
		\draw[thick] (R2) -- (R3) node[red,midway,sloped,above] {$\longleftarrow$};
		
		\draw[line width=5pt] (R2) -- (R5) node[red,midway,sloped,below] {$\longrightarrow$} node[blue,midway,sloped,above] {$\longrightarrow$};

The big line is a tunnel.

To manage the tunnel, we have to put in the routers :

- R3 : *FEC|PUSH(66), North*
- R2 : *66|SWAP(20), PUSH(123), East*
- R4 : *123|SWAP(33), North-East*
- R5 : 
	- *33|POP*
	- *20|SWAP(67), North-West*
- R6 : *67|SWAP(1), North-West*

With this solution, the :red:`red path` can be manage through the tunnel at the same time as the :blue:`blue path`.
When R2 receives a packet from R3, it swaps the label on the top of the stack (66) by 20, and push 123 on the top of the stack. So "20|123" is the stack, 123 being on the top. When the packet arrives to R4 (because the tunnel doesn't really exist), R4 swaps 123 by 33, and forwards it to R5. R5 pops 33, look up again in its table to manage the 20 on the top of the stack. It swaps so 20 by 67, and forwards it to 56.
For the blue path, we can use the same logic. R2 will use the tunnel and thus R4 don't need to add an entry for the blue path. We can see that these tunnels can reduce the size of the LFT when plenty LSPs go along the same set of LSRs.

But there is one drawback : R5 have to look twice in its table. Once when it has to manage the 33 on the top of the stack, and once when it has to manage the 20, which follow the 33 on the stack.
One solution is to use the *Penultimate Popping*.
With this solution we have this in the router table :

- R3 : *FEC|PUSH(66), North*
- R2 : *66|SWAP(20), PUSH(123), East*
- R4 : *123|POP, North-East*
- R5 : *20|SWAP(67), North-West*
- R6 : *67|SWAP(1), North-West*

With this solution, R5 don't need to look up twice in its forwarding table because R4 didn't swap, but pop the top of the stack.


Minimizing LFT size
===================

.. tikz:: Label stacking
		:libs: positioning, matrix, arrows
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		
		
		\matrix[row sep=2cm, column sep=2cm] {
			\node[router] (R1) {R1}; & \node[router] (R6) {R6}; & \node[router] (R8) {R8}; \\
			\node[router] (R4) {R4}; & \node[router] (R3) {R3}; & \node[router] (R5) {R5}; \\
		};
		
		\draw[thick] (R1) -- (R6) node[red,midway,sloped,above] {$\longrightarrow$};
		
		\draw[thick] (R6) -- (R8) node[red,near start,sloped,above] {$\longrightarrow$} node[gray,near end,sloped,above] {$\longrightarrow$} node[orange,near start,sloped,below] {$\longrightarrow$} node[blue,near end,sloped,below] {$\longrightarrow$};
		
		\draw[thick] (R4) -- (R3) node[orange,midway,sloped,above] {$\longrightarrow$};
		
		\draw[thick] (R3) -- (R6) node[orange,near start,sloped,above] {$\longrightarrow$} node[blue,near end,sloped,above] {$\longrightarrow$};
		
		\draw[thick] (R5) -- (R8) node[green,midway,sloped,above] {$\longrightarrow$};

.. role:: red
.. role:: green
.. role:: blue
.. role:: gray
.. role:: orange

In this diagram, we can regroup the five LSPs (the :red:`red`, the :orange:`orange`, the :gray:`gray`, the :green:`green` and the :blue:`blue`) as one LSP. e.g. to combine the :red:`red`, the :orange:`orange` and the :green:`green`, we put in the forwarding table of :

- R1 : *FEC|PUSH(123), North-East*
- R6 : *123|SWAP(8), East*
- R4 : *FEC|PUSH(5), North-East*
- R3 : *5|SWAP(123), North*
- R5 : *FEC|PUSH(8), North*
- R8 : *8|POP, East*


Create Label Switched Path
==========================

We can do it manually in each intermediate LSR, but it does not scale and we cannot assure all configurations are corrects and LSPs are active.

The solution is to use LSP unidirectional. The MPLS packets will follows the shortest path to reach the destination.

Destination-based forwardingI would start with the label forwarding instead.
============================

We use two different things to map labels onto FECs :

1. Labels Mapping (FEC -> label).
2. Label Distribution Protocol.

Labels Mapping
--------------

We can advertised the label mappings by two different methods :

1. Send it on all links so that upstream LSRs always know a label to reach a given destination :
	- (+) The cost on the CPU is low.
	- (+) In case of failure, it recovers faster because it only recomputes the shortest path to the destination, and the Label Forwarding Table (LFT) is changed according to the new shortest pathI would start with the label forwarding instead..
	- (-) There is a lot of LM message.
	- (-) It consumes a lot of memory.
2. Send it only on the shortest paths to reduce the number of mappings advertised :
	- (+) The number of LM message is reduced.
	- (+) It consumes less of memory.
	- (-) It has a high cost on the CPU to check if we are on the shortest path of our neighbors.
	- (-) In case of failure, we have to redo all the computation.


First solution
''''''''''''''

- A labelled packet may carry many labels, organized as a LIFO stack.
- Processing is always made on the top label.
- At any LSR, a label may be added to the stack (push operation) or removed from the stack (pop operation).

.. tikz:: Labels Mapping
		:libs: positioning, matrix, arrows
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		
		
		\matrix[row sep=3cm, column sep=2.5cm] {
			\node[router] (R1) {R1}; && \node[router] (R6) {R6}; && \node[router] (R8) {R8}; & \node[host] (p1) {p1};\\
			\node[router] (R4) {R4}; && \node[router] (R3) {R3}; && \node[router] (R5) {R5}; & \node[host] (p2) {p2};\\
		};
		
		\draw[thick] (p1) -- (R8);
		
		\draw[thick] (R1) -- (R6) node[green,near end,sloped,above] {$\longleftarrow$} node[green,very near end,sloped,above] {\scriptsize{LM(3, p1)}} node[purple,near start,sloped,above] {$\longrightarrow$} node[purple,very near start,sloped,above] {\scriptsize{LM(7, p1)}};
		
		\draw[thick] (R6) -- (R8) node[blue,near end,sloped,above] {$\longleftarrow$} node[blue,very near end,sloped,above] {\scriptsize{LM(7, p1)}} node[green,near start,sloped,above] {$\longrightarrow$} node[green,very near start,sloped,above] {\scriptsize{LM(3, p1)}};
		
		\draw[thick] (R1) -- (R4) node[purple,near start,sloped,above] {$\longrightarrow$} node[purple,very near start,allow upside down,above] {\scriptsize{LM(7, p1)}} node[orange,near end,sloped,above] {$\longleftarrow$} node[orange,very near end,allow upside down,above] {\scriptsize{LM(6, p1)}};
		
		\draw[thick] (R6) -- (R3) node[green,near start,sloped,above] {$\longrightarrow$} node[green,very near start,allow upside down,above] {\scriptsize{LM(7, p1)}};
		
		\draw[thick] (R5) -- (R8) node[blue,near end,sloped,above] {$\longleftarrow$} node[blue,very near end,allow upside down,above] {\scriptsize{LM(7, p1)}};
		
		\draw[thick] (R4) -- (R3) node[orange,near start,sloped,above] {$\longrightarrow$} node[orange,very near start,sloped,above] {\scriptsize{LM(6, p1)}};
		
		\draw[thick] (R3) -- (R5);
		
		\draw[thick] (R5) -- (p2);

.. role:: green
.. role:: blue
.. role:: purple
.. role:: orange

Chronological order of the figure :

1. :blue:`R8 knows it can reach p1, so it broadcasts the message LM(7, p1). It means "If you want to reach p1, please use label 7".`
2. :green:`R6 receives the LM message of R8, and sends to all its neighbour that it can reach p1, by sending the LM message LM(3, p1). R6 puts in its label forwarding table : 3|SWAP(7), East.`
3. :purple:`R1 receives the LM message from R6, learns that it can reaches p1, and sends it to all its neighbour with the message LM(7, p1). R1 puts in its label forwarding table : 7|SWAP(3), East.`
4. :orange:`Same logic. R4 puts in its label forwarding table : 6|SWAP(7), North.`

Second solution
'''''''''''''''

.. tikz:: Labels Mapping
		:libs: positioning, matrix, arrows
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		I would start with the label forwarding instead.
		
		\matrix[row sep=2cm, column sep=2cm] {
			\node[router] (R1) {R1}; && \node[router] (R6) {R6}; && \node[router] (R8) {R8}; & \node[host] (p1) {p1};\\
			\node[router] (R4) {R4}; && \node[router] (R3) {R3}; && \node[router] (R5) {R5}; & \node[host] (p2) {p2};\\
		};
		
		\draw[thick] (p1) -- (R8);
		
		\draw[thick] (R1) -- (R6) node[purple,midway,sloped,above] {$\longleftarrow$} node[purple,near end,sloped,above] {\scriptsize{LM(5, p2)}};
		
		\draw[thick] (R6) -- (R8) node[green,midway,sloped,above] {$\longleftarrow$} node[green,near end,sloped,above] {\scriptsize{LM(6, p2)}};
		
		\draw[thick] (R1) -- (R4) node[orange,midway,sloped,above] {$\longrightarrow$} node[orange,near start,allow upside down,above] {\scriptsize{LM(4, p2)}};
		
		\draw[thick] (R6) -- (R3) node[purple,midway,sloped,above] {$\longrightarrow$} node[purple,near start,allow upside down,above] {\scriptsize{LM(5, p2)}};
		
		\draw[thick] (R5) -- (R8) node[blue,midway,sloped,above] {$\longrightarrow$} node[blue,very near start,allow upside down,above] {\scriptsize{LM(7, p2)}};
		
		\draw[thick] (R4) -- (R3);
		
		\draw[thick] (R3) -- (R5);
		
		\draw[thick] (R5) -- (p2);

.. role:: green
.. role:: blue
.. role:: purple
.. role:: orange

Chronological order of the figure :

1. :blue:`R5 knows it can reach p2, so, it will advertise the other routers, but through the shortest path. So, it send the LM message LM(7,p2) to R8.`
2. :green:`R8 receives the message and will advertise the other routers through the shortest path, and send LM(6,p2) to R6. R8 store in its table : 6|SWAP(7), South.`
3. Etc.



Label Distribution Protocol (LDP)
=================================

Neighbour Label Switching Router (LSR) use UDP to discover whether the neighbour supports LDP.

.. tikz:: Labels Mapping
		:libs: positioning, matrix, arrows
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		
		
		\matrix[row sep=1cm, column sep=3cm] {
			\node[router] (R1) {R1}; & \node[router] (R2) {R2};\\
		};
		
		\draw[thick] (R1) -- (R2) node[red,near end,sloped,above] {$\longrightarrow$} node[red,near start,sloped,above] {\scriptsize{LDP Hello}} node[red,near start,sloped,below] {$\longleftarrow$} node[red,near end,sloped,below] {\scriptsize{LM(6, p2)}};

*NB : R1 and R2 supports MPLS.*

Neighbour LSRs maintain an LDP session over TCP connection as BGP sessions. Labels advertise remain usable as long as LDP session remains up. KEEPALIVE message to ensure that TCP connection remains up.

*NB : LDP and OSPF protocol are independent. They can cooperate together but LDP was design to be also used with other protocols like RIP. So, it has to have a special message to remove a label for a given prefix from a LFT.*

Link failures
-------------

When a link failure happens, the IP Routing protocol detects it, and updates IP routing tables by sending Labels Withdrawals (LW).

.. tikz:: Link failures
		:libs: positioning, matrix, arrows
		
		\tikzstyle{arrow} = [thick,->,>=stealth]
		\tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
		\tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
		
		
		\matrix[row sep=1cm, column sep=1cm] {
			\node[router] (R1) {R1}; & \node[router] (R6) {R6}; & \node[router] (R8) {R8}; & \node[host] (p1) {p1};\\
			\node[router] (R4) {R4}; & \node[router] (R3) {R3}; & \node[router] (R5) {R5}; & \node[host] (p2) {p2};\\
		};
		
		\draw[thick] (p1) -- (R8);
		
		\draw[thick] (R1) -- (R6);
		
		\draw[thick] (R6) -- (R8);
		
		\draw[thick] (R1) -- (R4);
		
		\draw[thick] (R6) -- (R3);
		
		\draw[thick] (R5) -- (R8);
		
		\draw[thick] (R4) -- (R3);
		
		\draw[thick] (R3) -- (R5);
		
		\draw[thick] (R5) -- (p2);

Suppose the Label Routing Tables are established. If, for example, the link between R8 and p1 fails, R8 will send LW(7,p1) to the other routers to say it cannot reach p1 anymore.

Label Switching Router (LSR) failures
-------------------------------------

When a router fails, the IP Routing protocol detects it, and updates IP routing tables. It is made by recomputing the path. The corrects routers has the two options :

1. The router receives the modification, and switch in its label forwarding table.
2. The router announces the modification, and then, switches in its label forwarding table.



What does MPLS bring?
======================

Connection-Oriented QoS Support
-------------------------------

A connection-oriented framework on a IP-based internet imposed by MPLS improves the sophisticated QoS support. The following are key requirements :

- Guarantee a fixed amount of capacity for specific applications, such as VoIP.
- Control latency and jitter and ensure capacity for voice.
- Provide very specific, guaranteed, and quantifiable service-level agreements, or traffic contracts.
- Configure varying degrees of QoS for multiple network customers.

This oriented connection is done by definition of a specific path through the network of LSRs for each Forwarding Equivalent Class (FEC).

Traffic Engineering
-------------------

Traffic Engineering is the ability to dynamically define routes, plan resource commitments on the basis of known demand, and to optimize network utilization. Thanks to MPLS, we can set up routes on the basis of these individual flows, with two different endpoints perhaps connected to different routers. 
Routes are changed on a flow-by-flow basis.
For more information, you can read the chapter dedicated to it (MPLS-TE)

VPN Support
-----------

MPLS makes it easy to commit network resources in such a way as to balance the load in the face of a given demand and to commit to differential levels of support to meet various user traffic requirements. The ability to dynamically define routes, plan resource commitments on the basis of known demand, and optimize network utilization is referred to as traffic engineering.
For more information, you can read the chapter dedicated to it (MPLS-VPN)

Multiprotocol support
---------------------

MPLS is configured so that it can coexist with other protocols. Therefore, MPLS-enabled routers can coexist with ordinary IP routers (or in ATM and Frame Relay networks), facilitating the introduction of evolution to MPLS schemes.


Label *Versus* IP packet forwarding
-----------------------------------

- The size of data structure on each node is smaller in MPLS.
- MPLS is faster. The computational cost of a lookup is so lower for MPLS.
- The expected performance are sensibly the same if it's a small bandwidth. But, MPLS should be faster for higher bandwidth.

