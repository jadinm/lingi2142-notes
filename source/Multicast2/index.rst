.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. sectionauthor:: Alexandre D'Hondt, Henri Crombe, Hussein Bahmad, Julie Chatelain, Mallory Declercq

Multicast routing protocols
===========================

This chapter will describe the operation of Multicast routing protocols.

.. toctree::
   :maxdepth: 1

   Multicast Routing <multicast-routing>
   IP Multicast <ip-multicast>
   Ethernet Multicast <ethernet-multicast>
   Internet Group Management Protocol <igmp>
   Flood and Prune Protocol <flood-prune-protocol>
   Multicast Open Shortest Path First <mospf>
   Core-Based Trees <cbt>
   Protocol Independent Multicast - Sparse Mode <pim>
   Any-Source and Source-Specific Multicast <asm-ssm>

   Multiple Choice Questions <mcq>

