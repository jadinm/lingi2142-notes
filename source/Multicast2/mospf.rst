.. sectionauthor:: Mallory Declercq

.. include:: <isonum.txt>

Multicast Open Shortest Path First
==================================

MOSPF is a protocol that extends OSPF [#fospfdef]_ Version 2 (:rfc:`1584`) to support multicast routing.

In MOSPF, the forwarding of multicast datagrams uses both the source and the destination address and a new type of LSA [#lsa]_ called `group-membership-LSA` has been added in order to track the location of each multicast group members. This new LSA ensures that multicast datagrams will be forwarded to each member of a group. Those LSAs are stored into the OSPF link state database. This database provides a complete description of the autonomous system's topology.

.. note:: The enhancements provided by MOSPF have been added in a backward compatible fashion and then routers running MOSF can interoperate with non-multicast OSPF routers when forwarding unicast IP data traffic.


How does it work ?
------------------

When a MOSPF router receives a multicast datagram (s |rarr| G), the router, based on its knowledge of the network topology and the members distribution inside the multicast groups, builts a spanning tree rooted at the source such that the path between the source and each destination along the tree corresponds to the the shortest path. The Dijkstra algorithm is used to compute the shortest path. It's important to note that the branches that do not contained routers involved in the shortest path are pruned from the tree.


Illustration
------------

This example covers the case where the source of the multicast datagram and the group members are inside the same area.

For a better understanding of this example, suppose that the hosts labelled Ma (respectively Mb) correspond to hosts that have joined the group A (respectively group B) via the IGMP protocol and the sources of the multicast datagrams are labelled H2, H3, H4, H5.

.. figure:: imgs/mospf-example.png
   :align: center
   :scale: 100

   Example of topology using MOSPF

**Case 1** : Forwarding when H2 sends a multicast datagram to group B.

1. H2 sends a multicast datagram to group B.

2. RT3 receives the multicast datagram and tries to forward it to the members of multicast group B. This is accomplished by sending a single copy of the datagram onto network N3.

3. Upon reception of the multicast datagram from RT3, routers RT1 and RT2 multicast the datagram on their subnetworks (N1 and N2 respectively).

**Case 2** : Forwarding when H2 sends a multicast datagram to group A.

1. H2 sends a multicast datagram to group 1.

2. RT3 receives the multicast datagram and creates 2 copies of it.

  a) One is sent onto network N3 which is then forwarded onto network N2 by RT2.

  b) The other copy is sent to router RT10 via RT6.

3. RT10 duplicates the datagram and sends it onto networks N6 and N11.


Disadvantage
------------

There is a `scalability issue` with MOSPF for networks with large numbers of multicast source-subnets. This is due to the fact that the computation of the Dijkstra algorithm can easily overwhelmed the routers.

.. rubric:: Footnotes

.. [#fospfdef] OSPF is a link-state routing protocol that provides a database that describes the Autonomous System's topology. Each router knows the entire network topology.

.. [#lsa] Link-State Advertisement

