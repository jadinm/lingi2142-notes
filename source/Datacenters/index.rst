.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Datacenters
===========

This chapter will describe the organisation of large datacenters and some networking techniques that they use.