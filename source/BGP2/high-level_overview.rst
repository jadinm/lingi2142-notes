High-Level Overview of Interdomain Traffic Engineering
------------------------------------------------------
.. sectionauthor:: Loic Schils

The aim of interdomain traffic engineering is to balance the traffic on links with other ASes and reduce the cost of traffic on these links. An AS will try to optimize the incoming and outgoing traffic regarding its business interests. Transit AS will try to balance the traffic on different links. It is done by choosing one link over another for a given destination. Several techniques can be used.

The examples of the following subsections are inspired by [QUP03]_.

Control of the Outgoing Traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An AS should be able to select the route that will be used to attain a certain destination. This can be done with two techniques that rely on:

- Local-pref attribute: Local-pref is an optional attribute distributed inside an AS. It is used to sort route by preference. It is the first criteria that is used in the BGP decision process.

.. figure:: figures/BGP2_LocalPref.png
   :align: center
   :scale: 50 

:Example:

	In the figure above, let's suppose that the link R1-R2 has a high bandwidth and the link R2-R3 has a low bandwidth. So in order to always use route learned from link R1-R2, the router of AS1 will insert a high local-pref to route learned from R1-R2 and insert a lower local-pref to route learned from R1-R3. The second rules in the BGP decision process (after the one that remove unreachable link) is that the link with the highest local-pref is prefered over another link, so as long as the link R1-R2 is reachable, this link will be used by AS1.

- Intradomain routing protocol: It influences how a packet crosses the transit ISP. For example, an AS can tune the weight of its intradomain routing protocol which will have an incidence on its outgoing traffic.

.. figure:: figures/BGP2_Intradomain.png
   :align: center
   :scale: 80

:Example:

	In the figure above, suppose that router R1 receive one packet whose destination is R8. The BGP decision process of router R1 will compare two routes towards R8. One that use R2 and the other that use R3. By choosing R2, we are sure that we consumne as few ressources as possible inside AS1. "If a transit relies on a tuning of the wieghts of its intradomain routing protocol, this tuning will indirectecly influence its outgoing traffic" [QUP03]_

Control of the Incoming Traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are several techniques that can influence the incoming traffic that rely on:

- Selective advertisements: It will communicate different route advertisements to different links. It is useful to balance the traffic.

.. figure:: figures/BGP2_Selective.png
   :align: center
   :scale: 50

:Example:

	In the figure above, AS2 want to balance the traffic coming from AS1. There are two links between AS1 and AS2: R4-R5 and R3-R6. A solution to balance the traffic could be to announced routes learned from AS3 on the link R3-R6 and announced its internal routes via R4-R5. So if AS1 want to contact AS3, the only routes possible is R3-R6. However, a drawback of this solution is that if the R3-R6 fails, AS1 will not be able to contact AS3.

- Selective advertisements with more specific prefix: An IP router always chooses in its forwarding table the most specific route (So, the route with the longest prefix)

.. figure:: figures/BGP2_Specific.png
   :align: center
   :scale: 80 

:Example:

	In a forwarding table that contains route towards 16.0.0.0/8 and 16.1.2.0/24, a packet destined to 16.1.2.200 will use the second link. In the figure above, suppose that the prefix 16.0.0.0/8 belong to AS2 and servers are also part of the subnet with the prefix 16.1.2.0/24. So if AS3 want to receive packets towards the link R3-R5 instead of other link that communicate with other AS, it can announced the two prefix on the link R3-R5 and only the prefix 16.0.0.0/8 on the other links. This solution is better than the previous one, because if R3-R5 fails, the subnet will still be reachable via the other links.

- Length of the AS-Path: It is done by increasing artificially the length of this attribute. It is a way to sort the outgoing route advertisement. By increasing the AS-Path attribute, it plays on the fact that it is the third criteria in the BGP decision process.

.. figure:: figures/BGP2_ASPath.png
   :align: center
   :scale: 50 

:Example: 

	In the figure above, the link R1-R2 is used as a primary link and the link R1-R3 as a backup link. In order to do that, AS1 will announced normaly its route on the link R1-R2 but will attached its own AS several times (instead of once in the AS-Path Attribute) on the backup link. On the BGP decision process, the third rules choose the route with the shortest AS-Path, so if local-pref is not used, the link R1-R2 will be choose as primary link.

- Combination of selective advertisements and length of the AS-Path.

:Example: 

	"An AS could divide its address space in two prefixes p1 and p2 and advertise prefix p1 without prepending and prefix p2 with prepending on its first link and the opposite of its second link." [QUP03]_

- Multi-exit-discriminator (MED): It is an optional attribute that can be only used in a situation where an AS multi-connected to another AS try to influence the route that this AS will use. Generally, the utlisation of MED need some negotation between two ASes.

Community-based
~~~~~~~~~~~~~~~

There is also the possibility to use communities to engineer the traffic. BGP communities will be seen in a following section.

Limitations
~~~~~~~~~~~

All these solutions have limitations:

The control of the outgoing traffic relying on the choice of the best route is limited by the route received from upstreams providers.

The incoming traffic based on tuning may cause several issues: 

- AS advertising more specific prefixes will advertise more prefixes than needed. On the global Internet this leads to a growth of the BGP routing table. Some ISPs will just ignore these advertisements. So this technique becomes less effective.
- The solution based on the manipulation of the AS-Path is tricky. It is often done by trial-and-error.

Another issue is that changing an attribute in the BGP advertisements will possibly be redistributed to the entire Internet. And then increase the number of BGP messages exchanged and lead to some instabilities.
