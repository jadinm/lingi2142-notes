.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Quality of Service
==================

This chapter will describe the main principles of the Integrated and Differentiated services and how these services are provided in IP networks. 

.. toctree::
   :maxdepth: 2

   The ReSource reserVation Protocol <rsvp>
   Integrated services <intserv>
   Differentiated services <diffserv>


The following reference might be a starting point for a comparison

Mathy, Laurent, Christopher Edwards, and David Hutchison. "The Internet: a global telecommunications solution?." Network, IEEE 14, no. 4 (2000): 46-57.
http://csis.pace.edu/~ctappert/dps/d861-06/pres-khaled2.pdf
