.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Network recovery techniques
===========================

This chapter will describe the techniques that are used to recover quickly from nodes and link failures.