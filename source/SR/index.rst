.. This document is part of the notes for the Computer Networks: Configuration and Management
.. course given at UCL

.. Use the section author directive to indicate the contributor to each section

Segment Routing
===============
:Authors: 
	Dederichs Francois
	Hardy Simon
	Klein Florent
	Scheerens Noel

This chapter will describe the operations of the recently proposed Segment Routing architecture. There are currently no published articles that describe Segment Routing. Most of the information on this topic is covered in Internet drafts and presentations, all of which are available at http://www.segment-routing.net

Among these documents, the following are the best starting points:

 - C. Filsfils, `Segment Routing - Simplifying the Network`, NANOG58, see http://www.nanog.org/meetings/nanog58/agenda
 - C. Filsfils et al., `Segment Routing Architecture`, https://datatracker.ietf.org/doc/draft-ietf-spring-segment-routing/
 - S. Previdi et al., `SPRING: Problem statement and requirements`, https://datatracker.ietf.org/doc/draft-ietf-spring-problem-statement/

============
Introduction
============

In this chapter, we will describe the Segment Routing architecture, a new approach allowing nodes to specify a particular forwarding path for the packets they send, bypassing the traditional shortest path that the packet would have followed if no *Segment* was put in it. Before diving into more technical stuff, it can be useful to define some vocabulary that will be used throughout this chapter: 

- *SPRING*: short for *Source Packet Routing in Networking*, it's a standard defined in :cite:`SPrevidi2015`. It's a general architecture defined in order to allow a node to specify a (unicast) forwarding path for packets, which can be different from the regular shortest path. 
- *Segment Routing*: it's kind of implementation of the SPRING architecture, defining in practice how the different prerequisites of the SPRING problem will be met. The way Segment Routing works is to put instructions (called *segments*) in the packet in order to tell it which way to go. The different types of segments will be more detailed later. 
- The *Source Routing* protocol is one of the predecessors of Segment Routing. Do not get them confused! This protocol allowed a sender to specify a particular route to be followed by the packets sent, but it didn't work quite well and had severe security issues, as we will see. 

In the following sections, we will first put things into context. After that, we will explain how Segment Routing routing works in practice: the different types of segments and examples. Then, we will describe both possible dataplanes for Segment Routing, and the possible security issues they should handle. Finally, we will dive more in depth into the requirements of the SPRING architecture, illustrated by some interesting use-cases that should be feasible. 

========================
Context and predecessors
========================

Before diving into Segment Routing, let's do a small reminder. There are two possible organizations for the network layer: 

- Datagram (each packet contains the source and destination address: uses forwarding tables);
- Virtual circuits (each packet contains the source address and a label: uses label switching through label forwarding tables). 

Virtual circuits allow us to take a specific path known from the beginning, but it has limitation and does not scale very well (although it is used in telephone networks), so let's focus on the datagram organization. The datagram, which is the most popular organization, can be divided into two planes:

- The data plane, which defines the structure of packets and forwarding tables (examples of data planes are IPv6 and MPLS);
- The control plane, which defines how to compute and maintain the forwarding tables via an intradomain routing protocol or IGP (short for Interior Gateway Protocol). 

It is possible to have several IGPs inside one network. Examples of IGPs are:

- Manual computing of routing tables;
- Distance vector routing;
- Link state routing. 

As a reminder, the most widespread IGP is link state routing (examples of such protocols are OSPF or IS-IS), mostly due to its scalability. Distance-vector routing is also used sometimes (its most famous protocol being RIP). 

Let's now quickly talk about Source Routing, and why it didn't see widespread adoption. Source routing is orthogonal to the IGP used, and it allows the user to change the path that a packet will take through a network by adding a list of nodes to the header of this packet. As a consequence, instead of following the shortest path to its destination, the packet will follow the shortest path to each of these nodes one after the other, before taking the shortest path to the destination. For example, in the following figure, the sender A could send a packet to D but impose that the packet first visits F, then C, then B and only then, D (green path). The structure of such a packet would look like this: 

[A -> D  |  F, C, B  |  Data]

He could also ask the packet to go to D, transiting by C using load-balancing (blue path). In this case, the structure of the packet would look like this: 

[A -> D  |  C  |  Data]

.. image:: pictures/SR-source_routing.png

But Source Routing didn't work quite well, especially due to security concerns: there was a severe trust issue regarding the hops (forming the path) imposed to the packet. For example, an attacker could easily create a Denial of Service attack targeting the bandwidth of a network by forcing packets to loop in the network, with a packet such as this one: 

[A -> D  |  F, E, F, E, F, E...  |  Data]

As a consequence, ISPs ended up disabling IPv4 and IPv6 Source Routing because of those security problems and this protocol was eventually forgotten. (For more information about these security issues, see :cite:`JABLEY2007`.)

Segment routing is the successor of Source Routing: it's a new protocol allowing the source to reach its destination in the network using *any* path of *any* shape. Unlike Source Routing (which was fully IGP), Segment Routing is an extension of another IGP like OSPF or IS-IS, but can work in any data plane (as far as it's able to carry the instructions, called *Segments*): the two data planes considered so far are IPv6 and MPLS. 

Note that Segment Routing can also be seen as the successor of the *Label Distribution Protocol* (LDP) and *Resource Reservation Protocol* (RSVP), which are protocols used in MPLS networks to establish label values between adjacent *Label Switched Routers* (LSR) in order to define a *Label Switched Path* (LSP). The LSP is then used by packets belonging to the related *Forwarding Equivalence Class* (FEC): it's a predefined path in the network and does a similar job as Source Routing and Segment Routing. However, Segment Routing is more general because it can work either in MPLS or in strict IPv6 networks, and it allows for a wider range of possibilities, as we will see in the following sections. 

=================
Types of segments
=================

Node Segments
-------------

An IGP-Node Segment is defined in the IETF draft of Segment Routing :cite:`CFILSFILSAL2015`  as *"an IGP-Prefix Segment which identifies a specific router"*. An IGP-Prefix Segment indicates a prefix, which is global within the SR/IGP domain, to which the packet will go through the ECMP-aware shortest path. An IGP-Node Segment is a particular case of IGP-Prefix Segment that indicates a single node instead of a prefix.
'Node Segment' or 'Node-SID' are widespread abbreviations for an IGP-Node Segment.
When a Node Segment is being used on a packet, this packet will follow the ECMP-aware shortest path towards the specified node. The prefix of any Node Segment cannot be owned or advertised by more than one router within the routing domain.

Let's look at the following network as an example, in which we want to send packets from R2 to R7:

.. image:: pictures/SR-2.png

The red path is easily described by Node Segments, with a single segment indicating R1 or R3. Note that this path was already partially used by the IGP (with load-balancing), but this SR allows to break ECMP and use only this red path. 

The green path, on the other hand, cannot be taken using only Node Segments. Since the packets follow the ECMP-aware shortest path to reach the indicated node, it is not possible to force the traffic to take the direct R3 -> R5 route (which costs 100) instead of making a detour by R7 (which is its actual final destination!) for a route of cost 2.
In other words, even if you give the packets the Node Segments to go to R1 then R3 then R5 and finally R7, the actual route they will follow is R1 -> R3 -> R7 -> R5 -> R7, which kind of defeats the purpose.

The blue path is problematic as well. Since the packets will always follow the ECMP-aware shortest path, it is not possible to do load balancing between two routes of different costs.

Adjacency Segments
------------------

As stated in the IETF draft on Segment Routing:
'An "IGP Adjacency Segment" [...] enforces the switching of the packet from a node towards a defined interface or set of interfaces. This is key to theoretically prove that any path can be expressed as a list of segments [...]' :cite:`CFILSFILSAL2015` .Furthermore, by default an Adjacency Segment is local to the node that advertises it. However, the Adjacency Segment can be global if it is advertised as such. Furthermore, local Adjacency Segments have to always be preceeded by a Node Segment.

Then why use Node Segments at all if you can use global Adjacency Segments which are more powerful than Node Segments? There are several reasons to discourage the use of a global Adjacency Segment in replacement of a Node Segment:
	- The impact that they would have on the control plane and the Dijkstra computation. When using global adjacency segments, each router needs to potentially compute a specific path towards each link adjacency segment (because implicitly such a path must always pass through the attached router). This increases the cost of performing the Dijkstra computation.
	- We already have the shortest path to all the nodes computed and we don't want to recalculate it for each Adjacency Segments.
	- We don't want to have every Adjacency Segment Label in the FIB (Forwarding Information Base) Table of every router in the network since this would lead to scalability issues by bloating the FIB table.
	- We don't need to use globally unique labels for local Adjacency Segment. Those labels can then be reused inside the network which improves the network scalability. 

In other words, "The use of global Adj-SID allows to reduce the size of the segment list when expressing a path at the cost of additional state (i.e.: the global Adj-SID will be inserted by all routers within the area in their forwarding table)." :cite:`CFILSFILSAL2015` . So the use of global Adjacency Segments is strongly discouraged and by default, when actually we talk about Adjacency Segments, we refer to local Adjacency Segments.

Another advantage of using an Adjacency Segment is that they are forwarded without shortest-path consideration and if the Adjacency Segment label identifies a set of adjacencies, the traffic is split between the members of this set resulting in load balancing.

We will now revisit the example given above using Adjacency Segments.

.. image:: pictures/SR-2-Adjacency-Variant.png

We already know that we can do the red path using Node Segments.

Now that we have the Adjacency Segment we can also describe the green path with a first Node Segment indicating R3 and then an Adjacency Segment indicating the interface of R3 towards R5 (35). Since the Adjacency Segment ignores the shortest-path and are interface specific, we won't run into the same problem as previously. 

The load balancing of the blue path can either be done with Anycast Segments (explained hereafter) or Adjacency Segments. To achieve this path using Adjacency Segments, you will define a label regrouping the interfaces of R2 towards R4 and R8 (248). The path is now described as an Adjacency Segment (248) followed by a Node Segment indicating either R8 or R9.


While we can use Adjacency Segments to do load balancing, it isn't an efficient way to do it since it requires the traffic to arrive at a single router!

Anycast Segments
----------------

We stated previously that Node Segments had to have globally unique labels. But what would happen if two or more nodes had the same Node Segment Label ? Since the nodes use the shortest-path algorithm to choose the path on which to send packets towards a specific node label, the packets will be sent to the closest node using that label. This is the principle behind an Anycast Segment!

An Anycast Segment is used to specify that you have to send the packet from one router to a set of routers. That packet will be sent to the router of the set which has the shortest path. In case of a tie, the traffic will be load balanced between the closest routers and those two properties are what we are looking for!
We will now revisit the example given above using an Anycast Segment to describe the blue path.

.. image:: pictures/SR-2-Anycast-Variant.png

The blue path can be described by an Anycast Segment (label 42 shared by R4 and R8) followed by a Node Segment indicating either R8 or R9.

This is a much nicer way to do load balancing than using an Adjacency Segment, since the traffic can arrive from any number of path and be distributed to the nodes included in the set of the Anycast Segment. To achieve this, you need a set of routers with an anycast label at the same shortest path from the ingress of the traffic to load balance. This will allow us to distribute the traffic upon the set of routers.

Other Segment types
-------------------

There are types of segments we didn't talk about such as Binding Segments, BGP Peering Segments, ... However, they are quite specific and some of them are not standardized yet, so we won't talk about them here. For more information about those segments see: :cite:`CFILSFILSAL2015` .

RSVP-TE and LDP
---------------

.. image:: pictures/SR-2.png

We can now better compare Segment Routing with the two other protocols mentionned above, i.e. RSVP-TE and LDP. RSVP-TE allows to take any of the three paths specified in the previous figures because there is no restriction like the shortest path for Node Segments. On the other hand, LDP is destination-based so it can not take the green and the blue path of the above figures. In fact, there are only 3 paths that can be followed by LDP from R2 to R7 : the 3 equal-cost paths with a cost of 3, so Segment Routing is much more general than LDP. 

===========
Data planes
===========

The Data plane defines the structure of packets and forwarding tables. Here, we are going to explain the main differences between the MPLS and the IPv6 dataplanes for Segment Routing.

- *MPLS*: In the MPLS dataplane, a Segment List is represented as a stack of labels (20 bits each). The current segment is at the top of the stack and when the segment is completed, it must be popped from the stack. We can assume that each router is already assigned a *unique* MPLS label (in practice, they are already advertised by OSPF): in consequence, no modification is required in MPLS to deploy it. 

- *IPv6*: In the IPv6 dataplane, a Segment List is represented as an ordered list of IPv6 addresses. Unlike MPLS, the current segment is always referenced by a pointer. This implementation implies that the payload will have a static size all along its entire path. It also implies a smaller computation cost because it's easier to increment the value of a pointer than to move all the data, especially with an hardware implementation. Note that in MPLS, moving data wasn't as costly as with IPv6 because it was smaller (20 bits versus 128 bits). Note also that we can use a *key-hashed message authentication code* (HMAC) in order to secure the Segment List (more on this in the security section). 


===============
Security issues
===============

We previously saw that Source Routing had faced several security issues. One important goal of Segment Routing was to find ways to handle these issues. For example, in order to deal with the *trust issue* :cite:`VYNCKE2014`: 

- A host cannot specify a source route: only routers have that privilege;
- In the IPv6 dataplane only, there is an authentication mechanism based on a *key-hashed message authentication code* (HMAC). More precisely, two fields were added to the Segment Routing Header (SRH): 

	- HMAC Key-id (8 bits): identifies the hash algorithm and the type of pre-shard key. Set to 0 if HMAC isn't being used. 
	- HMAC (256 bits): output of the HMAC computation. The computation is based on the Key-id field and on the concatenation of some pieces of informations in the header (among others, the source IPv6 address and all the addresses in the Segment List). 

Thanks to this HMAC, we can verify the validity, the integrity and the authorization of the SRH. Indeed, an outsider of the SR domain doesn't have access to the pre-shared key, so it cannot compute the right HMAC field and if he tries to change it, the SR router that will process this SRH will detect that the HMAC is not valid and reject the packet. 
Nevertheless, the source and the destination need a pre-shared key. 

Note that the HMAC can have some performance impact, but given that it only needs to be checked at the entry of the Segment Routing domain (the ingress SR router), this impact is limited (moreover, the router at the entry can use a cache to store recent HMACs). 

With MPLS dataplane, we don't need that HMAC but we still have the same security issues as for traditional MPLS. However, SR doesn't add new security considerations to MPLS. 

Use cases
---------

The SPRING architecture should be able to handle the following use cases: 

1. Failure protection
 
Note: to react quickly enough to failure, routers often need to pre-compute the Forwarding Information Base (FIB). 

.. image:: pictures/SR-4.png

2. Loop Free Alternate

The Loop Free Alternate (LFA) is defined in :cite: `AATLAS2008`. It's a method to configure a router so that when a link connected to the router fail, the router will reroute the traffic to a neighbour. The choice of the neighbour depends on the destination of the packet since the router needs a neighbour that won't send that packet back to you to reach the destination since the packet would loop. The motivation behind such a method is that the router can react to the link failure a lot faster than the network.

There is also Remote LFA which uses the same idea but for links to which you aren't directly connected. The idea is to use Node and Adjacency Segments to reroute the traffic to avoid the remote failed link. 

Example: In the figure below, R2 wants to sends a packet to R8 and detects that the link R1-R4 is down. R2 can't use standard LFA since the link isn't connected to the router. R2 will then use Remote LFA by adding a Node Segment indicating R7 to the packet.

.. image:: pictures/SR-5.png

3. Dual-plane networks
 
These are frequent architectures, especially in national networks. In a dual-plane network, we want some independance between the two planes, so we only have a limited number of connections between them because the inter-plane cost is very high. 

.. image:: pictures/SR-6.png

4. Load balancing and disjoint paths

When A sends a packet to B, it must be possible to do some load balancing on both planes (and also inside a single plane), for example using anycast segment between R1 and RA (where R1 and RA share the same label). Let's configure the network as such (solution taken from :cite:`CFILSFILUSE2015`): 

	- The six routers (R1, R2, R3, R4, R5, R6) with an anycast loopback address 192.0.1.1/32 and an anycast-SID 101
	- The six routers (RA, RB, RC, RD, RE, RF) with an anycast loopback address 192.0.1.2/32 and an anycast-SID 102
	- The edge router A with a Node-SID 103
	- The edge router B with a Node-SID 104

A is now able to control his traffic in 3 different ways using Anycast Segments: 

	- {104}: the traffic is load-balanced across any ECMP path through the network
	- {101,104}: the traffic is load-balanced across any ECMP path within the panel of the network
	- {102,104}: the traffic is load-balanced across any ECMP path within the panel of the network


.. image:: pictures/SR-7.png

6. Crazy paths

Segment routing allows us to set some crazy paths inside a network. This could be useful to check congestion, queuing, latency and so on.

It's also possible to define paths that loop in the network. To define the red path on the following figure, we could for example use 9 Node Segments like this: {R1, R4, R5, R7, R9, R3, R1, RA, A}. 

.. image:: pictures/SR-8.png


.. bibliography:: references.bib

