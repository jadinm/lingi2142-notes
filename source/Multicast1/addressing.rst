.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Multicast Addressing
====================

.. sectionauthor:: Syed Mohammad
.. sectionauthor:: Detaille Mattieu


Multicast address are used inside a network to identify a group of hosts bound to a multicast service. We distinguish two type of multicast address depending on wich layer of the Open Systems Interconnection (OSI) model they are used : Ethernet multicast addresses on the Datalink layer and IP multicast addresses on the Network layer.

.. figure:: png/multicast-figures-001.png
   :align: center
   :scale: 80

   The seven layers of the OSI reference model

IP multicast addresses
----------------------

In IP addressing, we will of course distinguish IPv4 and IPv6.

IPv4
~~~~
IPv4 multicast 32-bits adresses are class D adresses (4 higher order bits set to 1110) and range only from 224.0.0.0 to 239.255.255.255. The Internet Assigned Numbers Authority (IANA) has assigned some subsets of these addresses to specific purposes as described in :rfc:`5771`.

IPv6
~~~~

IPv6 multicast address are composed of 128 bits divided as given in the following figure:

.. figure:: png/multicast-figures-002.png
   :align: center
   :scale: 80

   IPv6 multicast address format

See the above figure reference : [CM2004]_.

We distinguish 4 parts:
 + *Address type* (8 bits): IPv6 multicast addresses all have this field set to the hex *FF*.
 + *Flags* (4 bits): There are 3 distinct flags of 1 bit. The leftmost bit(MSB) has no role yet, it is reserved for future use. The 3 remaining flags are defined as follow (from the leftmost to the rightmost):
    - **R**: if 1, indicates that a rendezvous-point(RP) address is embedded in the group address. This flag is mainly used with the PIM-SM(SM = Sparse Mode) routing protocol that employs a specially configured RP router.
    - **P**: if 1, indicates that the multicast address was derived from a unicast-based network prefix.
    - **T**: if 1, indicates a temporarily assigned address.
 + *Scope* (4 bits): this field indicates the distance a multicast packet can travel. It prevents multicast packets from crossing the administrative network boundaries and from traversing links and networks in which they don't belong.
 + *Group ID* (112 bits): this field identifies a specific multicast group address in the network.

As for IPv4, IANA has reseved some addresses for specific use as described in :rfc:`4291` [#fmultiiana]_.

We were talking earlier about a **P** flag in the IPv6 multicast address. The following image show the format of a IPv6 multicast address derived from a unicast-based network prefix:

.. figure:: png/multicast-figures-003.png
   :align: center
   :scale: 80 

   Unicast-Prefix-Based IPv6 multicast address format (a) without (b) with embedded RP address.

See the above figure reference : [CM2004]_.

We can see in the image (a) that flag P must be set to 1 and that, beside the common *Address type*, *Flag* and *Scope*, there are the fields :
 + *Rsvd* (8 bits): reserved field, set to 0.
 + *plen* (8 bits): indicates the number of significant bits in the network prefix; the length of the prefix.
 + *Unicast network prefix* (64 bits): the network prefix assigned to a provider
 + *Group ID* (32 bits):this field identifies a specific multicast group address in the network like in the normal IPv6 multicast address. Nevertheless, it is shorter and thus allow less group in the network.

The figure (b) show a embedded RP in the multicast group address. The flag R and P must be set to 1. We see that the *Rsvd* field of figure (a) was divided in 2 fields : *Rsvd* (4 bist) and *RIID* (4 bits). To recompose the address of the embedded RP, one must proceed as follow :
  1. Take the *plen* bits of the *Unicast network prefix* and make them the MSBs(leftmost bist).
  2. Add zero padding after the *plen* bits of the *Unicast network prefix* up to 128 bits.
  3. Replace the 4 LSBs(rightmost bits) of the address with the content of the *RIID* field.

For example, the IPv6 group address FFxx:0A30:3FFE:FFFF:0001::1234 yield the RP address 3FFE:FFFF:0001::000A where 3FFE:FFFF:0001/48 is the *Unicast network prefix* with a *plen* of 48 (0x30). This way of addressing is used for protocol as PIM-SM and other using embedded RP.

Ethernet multicast addresses
----------------------------

Ethernet addresses are composed of 48 bits. Any address with its leftmost octet equal to 1 (e.g: 01:xx:xx:xx:xx:xx) is treated as a multicast addresse, while the address FF:FF:FF:FF:FF:FF is refered as the broadcast address. A frame with any of these two kind of addresses (multicast or broadcast) is flooded to the network. A host will listen the network, capture the frames he's interested in, then transfer them to an upper layer of the OSI model.

Note that IP multicast packets can be delivered using Ehternet addressing. The mapping between IPv4 and Ethernet is done as follow: we take the 23 lower bits (rightmost) of the IPv4 multicast group 32-bits address and map them to the 23 lower bits of the Ethernet 48-bist address. There is a risk of collison if the IPv4 multicast group address of two host only differ in the first 9 bits. Ethernet multicast addresses derived from an IPv4 address range from 01:00:5E:00:00:00 to 01:00:5e:7F:FF:FF (01:00:5E/24 is a reserved prefix for ethernet addresses mapped from IPv4)

The mapping between IPv6 and Ethernet is done as follow: we take the 4 lower octet of the IPv6 address and apply an *OR* operation with the specific address 33:33:00:00:00:00 (reserved for ethernet addresses mapped from IPv6). For example, the IPv6 address FEED:DEAD:BEEF::1:2:3 will map to the Ethernet address 33:33:00:01:02:03.

.. rubric:: Footnotes

.. [#fmultiiana] The full list of allocated IPv6 multicast addresses is available at http://www.iana.org/assignments/ipv6-multicast-addresses
.. [CM2004] C. Metz and M. Tatipamula, `A look at native IPv6 multicast`, Internet Computing, IEEE, no. 4, 2004.

