.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Group membership protocols for IP multicast
===========================================

.. sectionauthor:: Syed Mohammad
.. sectionauthor:: Detaille Mattieu

There exists two main Group membership protocols for IP multicast. The first one is the Internet Group Management Protocol(IGMP). The second one is the Multicast Listener Discovery Protocol(MLD). The two protocols are used by hosts and routers which are providing IP multicast services. Once the routers learn these group membership information, they can pass it to the multicast routing protocol(for example Protocol-Independent Multicast(PIM)) The routers in the network will then process multicast routing control packets to build the tree required to deliver multicast packets from the source to the multiple destinations. 

IGMP
----
This protocol is used with IPv4 addresses. It is used in order to exchange group membership information. 

To receive data from the multicast group, hosts must join the group by contacting their routers using the IGMP version 2. Once a host joins a group, it receives all data sent to the group address regardless of the sender's source address.  

IGMP is used by hosts to announce their interest in receiving a multicast group to edge routers. These edge routers use multicast routing protocols to form multicast spanning trees through the internet. 

Operations of the IGMP protocol can be summarized as follows(for an ethernet interface) :

  * When a host joins a group, it programs its ethernet interface to accept the relevant traffic, and it sends an IGMP Join message on its local network. This message informs the local routers that there is a receiver for this group on this subnet. 
  * The local routers remember this information, and arrange for traffic destined for this address to be delivered to the subnet. 
  * In order to wonder if there is still any member on the subnet, the routers send an IGMP query message to the multicast group. If there is still a host who is member of the group, it replies with a new message unless it hears someone else do so first. Multicast traffic continues to be delivered. 
  * If the application finishes and the host no longer wants the traffic, it reprograms its ethernet interface to reject the traffic. However, the packets are still sent until the router times the group out and sends a query to which no one responds. The router then stops delivering traffic.  

We thus observe that joining a group is quick but leaving can be slow with IGMP version 1. The version 2 reduces the leave latency by introducing a "leave" message and a set of rules to prevent one receiver from disconnecting others when it leaves. 

There is actually two working versions of IGMP. The first version was proposed in conjunction with DVMRP(flood-and-prune protocol), the first multicast routing protocol. The second version adds fast termination of group subscriptions and is in the default architecture of routers today. There is also a third version which is in development. It will allow receivers to subscribe to specific sources of a particular multicast group(at the expense of more complexity and extra state in routers). It will aslo provide source pruning for specific multicast groups. It will thus prevent data from entering the backbone when the routing protocols support this option(Not possible for shared tree protocols such as CBT or BGMP). However, attacks may still be possible in the backbone with IGMP version 3 if one receiver does not prune all noisy or malicious sources. To prevent such a scenario, receivers will have to explicitly subscribe to a known source list rather than prune noisy sources after the fact.


MLD
---
This protocol is used with IPv6 addresses. This protocol lets routers discover destination multicast hosts on directly attached subnets as well as their respective multicast group addresses of interest. It is indeed possible for a host using MLD protocol to signal to the router its desire to listen to(or remove itself from) a multicast stream identified by a specific group address.

It is also possible to have a source-specific multicast(SSM) with the MLD protocol. The host will then signal a group address and the multicast sender's associated source unicast IPv6 address. 

One of the advantage of MLD protocol is that it can be possible to conserve network resources by blocking multicast packets destined for ports at which hosts don't want to receive multicast packets. It can be possible thanks to ethernet switches which are snooping on MLD packets in order to learn the locations of routers and multicast hosts. 

Operations of the MLD protocol can be summarized as follows in the case of a host who wants to join an IPv6 multicast group : 
  * A host joins an IPv6 multicast group by sending an unsolicited MLD report or by sending a MLD report in response to a general query from an IPv6 multicast router. The switch snoops these reports.
  * In response to a MLD report, the switch creates an entry in its layer 2 forwarding table for the VLAN on which the report was received. When there is other hosts interested in joining the multicast group who are sending MLD reports, the switch snoops their reports and adds them to the existing Layer 2 forwarding table entry. The switch creates one entry per VLAN in the Layer 2 forwarding table for each multicast group for which it snoops an MLD report. 
  * The MLD snooping sends the MLD report of the host for the multicast group he wants to listen, to the IPv6 multicast router. 
  * The switch forwards finally the multicast traffic of the multicast group the host wanted to listen by reading the multicast group specified in the report and by forwarding its traffic to the interfaces where reports were received.  

Operations of the MLD protocol can be summarized as follows in the case of a host who wants to leave an IPv6 multicast group : 
  * Interested hosts in the multicast traffic must continue to respond to periodic MLD general queries. As long as at least one host in the VLAN answers to the MLD genera queries, the multicast router will continue to forward the multicast traffic to the VLAN. 
  * When a host wants to leave a multicast group, he can simply ignore the periodic MLD general queries of the multicast router. He can also sends an MLD filter mode change record. 
  * When MLD snooping receives a filter mode change record from a host with the EXCLUDE mode for a specific group, MLD snooping sends out a MAC-addressed general query to determine if any other hosts connected to that interface are interested in traffic for the specified multicast group. If MLD snooping does not receive an MLD report in response to the general query, it assumes that no other hosts connected to the interface are interested in receiving the multicast traffic from the specified group. It thus removes the interface from its Layer 2 forwarding table entry for the specified group.
  * If the filter mode change record was from the only remaining interface with hosts interested in the group, and if MLD snooping does not receive an MLD report in response to the general query, it removes the group entry and sends the MLD filter mode change record to the multicast router. If the multicast router doesn't receive anymore reports from a VLAN, it removes the group for the VLAN.

There is actually two versions of MLD protocol available. The first version corresponds to version 2 of IGMP. The second version adds support for source-specific multicast and corresponds to version 3 of IGMP which is actually in development.
