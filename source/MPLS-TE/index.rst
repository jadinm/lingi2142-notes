.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section
.. _`MPLS-TE`:

MPLS Traffic Engineering
========================

This chapter will describe how Traffic Engineering can be deployed in MPLS networks.

.. include:: ./intro.rst
.. include:: ./OSPF-TE.rst
.. include:: ./MPLS-TE_in_the_Internet.rst
.. include:: ./MPLS_DiffServ-TE_InterDomain.rst
