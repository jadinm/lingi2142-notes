
Question 1
~~~~~~~~~~
.. question::
   :nb_prop: 3 
   :nb_pos: 1
	
   Which router use the MPBGP protocol?	

   .. positive:: PE routers use MP-BGP protocol to exchange routes. 


   .. negative:: CE routers use MP-BGP protocol to exchange routes. 


   .. negative:: P routers use MP-BGP protocol to exchange routes. 

      
Question 2
~~~~~~~~~~
.. question::
   :nb_prop: 3 
   :nb_pos: 1
	
   When a PE router receives a new route, which technique allows it to know in which VRF it must be add this route? 	

   .. negative:: It's the Route Distinguisher that allows it.

      .. comment:: What if the route comes from the CE routers?
      
   .. positive:: There is no enough information to answer this question. 

   .. negative:: It's the Route Target that allows it.

      .. comment:: The Route Target is used to allows different client to use same addresses.
      
Question 3
~~~~~~~~~~
.. question::
   :nb_prop: 3 
   :nb_pos: 1
	
   Which protocol is used to forward VPN packet in the backbone area of the ISP

   .. negative:: It's the RSVP-TE protocol.

   .. negative:: It's the LDP protocol.
      
   .. positive:: It's the MPLS. 

Question 4
~~~~~~~~~~
.. question::
   :nb_prop: 2 
   :nb_pos: 1
	
   Can different VPN use similar addresses?

   .. negative:: No.

   .. positive:: Yes.

Question 5
~~~~~~~~~~
.. question::
   :nb_prop: 3 
   :nb_pos: 1
	
   Imagine a VPN where there are two PE routers connected to the same site
   a Route Reflector is used to advertise route and 
   the Route Distinguinsher is used by VPN. Through which router the 
   incoming traffic will come ?

   .. positive:: Only one of the PE (depend of the network information).

   .. negative:: Load balancing on the two routers.

   .. positive:: Depend of the position of the sender because it's the closest router.


